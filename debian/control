Source: python-srp
Maintainer: Ben Carrillo <ben@futeisha.org>
Uploaders: Micah Anderson <micah@debian.org>
Section: python
Priority: optional
Build-Depends:
 python-all-dev (>= 2.6.6-3),
 python3-all-dev,
 debhelper (>= 9),
 python-sphinx (>= 1.0.7+dfsg),
 libssl-dev
Standards-Version: 3.9.4
X-Python-Version: >= 2.6
X-Python3-Version: >= 3.0
Vcs-Git: https://github.com/cocagne/pysrp.git 
Vcs-Browser: https://github.com/cocagne/pysrp
Homepage: http://code.google.com/p/pysrp/

Package: python-srp
Architecture: any
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
 ${sphinxdoc:Depends},
 libssl1.0.0 | libssl1.0.2
Description: Secure Remote Password protocol implementation
 This package provides an implementation of the Secure Remote Password
 protocol (SRP). SRP is a cryptographically strong authentication
 protocol for password-based, mutual authentication over an insecure
 network connection.
 .
 Unlike other common challenge-response autentication protocols, such
 as Kereros and SSL, SRP does not rely on an external infrastructure
 of trusted key servers or certificate management. Instead, SRP server
 applications use verification keys derived from each user's password
 to determine the authenticity of a network connection.
 .
 SRP provides mutual-authentication in that successful authentication
 requires both sides of the connection to have knowledge of the
 user's password. If the client side lacks the user's password or the
 server side lacks the proper verification key, the authentication will
 fail.
 .
 Unlike SSL, SRP does not directly encrypt all data flowing through
 the authenticated connection. However, successful authentication does
 result in a cryptographically strong shared key that can be used
 for symmetric-key encryption.

Package: python3-srp
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
 ${sphinxdoc:Depends},
 libssl1.0.0
Description: Secure Remote Password protocol implementation
 This package provides an implementation of the Secure Remote Password
 protocol (SRP). SRP is a cryptographically strong authentication
 protocol for password-based, mutual authentication over an insecure
 network connection.
 .
 Unlike other common challenge-response autentication protocols, such
 as Kereros and SSL, SRP does not rely on an external infrastructure
 of trusted key servers or certificate management. Instead, SRP server
 applications use verification keys derived from each user's password
 to determine the authenticity of a network connection.
 .
 SRP provides mutual-authentication in that successful authentication
 requires both sides of the connection to have knowledge of the
 user's password. If the client side lacks the user's password or the
 server side lacks the proper verification key, the authentication will
 fail.
 .
 Unlike SSL, SRP does not directly encrypt all data flowing through
 the authenticated connection. However, successful authentication does
 result in a cryptographically strong shared key that can be used
 for symmetric-key encryption. This package contains the python3 version.
 .
 This package provides the python 3 version.
